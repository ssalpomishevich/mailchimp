<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\MemberRequest;
use App\Member;
use App\Http\Controllers\Controller;
use Newsletter;

class MemberController extends Controller
{

    public function index()
    {
        $members = Member::all();

        return response()->json(['data' => $members->all()]);
    }

    public function store(MemberRequest $request)
    {

        $member = Member::create($request->all());

        if ($member) {

            //Newsletter::subscribe($member->email, ['firstName' => $member->first_name, 'lastName' => $member->last_name]);

            return response()->json(['data' => $member]);
        }

        return response()->json([
            'data' => [
                'status' => false,
                'message' => 'Oops! Something went wrong while adding new member!'
            ]
        ]);
    }

    public function update(MemberRequest $request, $email)
    {
        $member = Member::find($email);

        if ($member->update($request->all())) {
            return response()->json(['data' => $member]);
        }

        return response()->json(['data' => 'Something went wrong while updating current member!']);

    }

    public function show($email)
    {
        $member = Member::find($email);

        return response()->json(['data' => $member]);
    }


    public function destroy($email)
    {
        $member = Member::find($email);

        if ($member->delete()) {
            return response()->json(['data' => 'Current member removed']);
        }

        return response()->json(['data' => 'Something went wrong while updating current member!']);
    }
}
