<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    // Name of the members table
    protected $table = 'members';

    // Table fields
    protected $fillable = [
        'first_name',
        'last_name',
        'email'
    ];
}
